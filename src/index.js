import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './container/App';
import {createStore, applyMiddleware, combineReducers, compose} from "redux";
import  thunkMiddleware from "redux-thunk";
import {Provider} from "react-redux"
import * as serviceWorker from './serviceWorker';
import dishes from "./store/reducers/dishesReducer";
import cart from "./store/reducers/cartReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducer = combineReducers({
    dishes: dishes,
    cart:cart
});


const store = createStore(rootReducer,composeEnhancers( applyMiddleware(thunkMiddleware)));


ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

serviceWorker.unregister();
