import React, {Component} from "react";
import {connect} from "react-redux";
import {closeModal, createOrder, deleteProduct, openModal, takeUserInfo} from "../../store/action";
import Modal from "../../components/UI/Modal/Modal";


class Cart extends  Component {


    render() {
            let carts;
            if(this.props.cart !== null) {
                carts = Object.keys(this.props.cart).map(cart => {
                    const info = this.props.cart[cart];
                    return   <div className="Dish"  key={cart} onClick={()=> this.props.delProduct( cart,info)}>
                        <span style={{paddingLeft:"71px"}}>
                              <h1>{cart}</h1>
                            <h2>x{info.cal}</h2>
                        <h3>cost: {info.cost*info.cal} KZ</h3>
                        </span>
                    </div>
                })
            }
        return (
            <>
            <Modal
            show={this.props.purchasing}
            closed={this.props.purchaseCancelHandler}
            createOrder={this.props.createOrder}
            takeInfo ={this.props.takeInfo}
            cart={this.props.cart}
            infoUser={this.props.infoUser}
            />
            <div style={{width: "300px", border:" 2px solid"}}>
              <h1 style={{    borderBottom:" 2px solid", paddingBottom: "10px"}}>Cart</h1>
                {carts}
                <span>
                       <h3>delivery: 100</h3>
                <h3>TotalPrice:{this.props.totalPrice}</h3>
                </span>

                {(this.props.totalPrice > 100)?
                    <button style={{marginBottom:"10px", width:"150px"}} onClick={() => this.props.makeOrder(this.props.cart)}>Place order</button>
                    :null}

            </div>
            </>
        )
    }
}
const mapStateToProps = state =>{
    return{
        cart: state.cart.cart,
        totalPrice:state.cart.totalPrice,
        purchasing:state.cart.purchasing,
        infoUser:state.cart.infoUser
    }

};
const mapDispatchToProps = dispatch =>{

    return{
        delProduct:(name, info) => dispatch(deleteProduct(name, info)),
        makeOrder:(info) => dispatch(openModal(info)),
        purchaseCancelHandler: () => dispatch(closeModal()),
        createOrder:(cart, info) => dispatch(createOrder(cart, info)),
        takeInfo: (name, value) => dispatch(takeUserInfo(name, value))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Cart);
