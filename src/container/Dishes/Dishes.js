import React, {Component} from "react";
// import Spin from "../components/Spinner/Spiner"
import {takeDishes, addProduct} from "../../store/action";
import {connect} from "react-redux";
import  "./dishes.css"
import Spinner from "../../components/UI/Spinner/Spinner";


class Dishes extends  Component {
    componentDidMount() {
        this.props.takeDishes()
    }


    render() {

        let dish;
        if(this.props.dishes !== null) {
            dish = Object.keys(this.props.dishes).map(dish => {
                const info = this.props.dishes[dish];
                return   <div className="Dish"  key={dish}>
                    <img style={{width: "100px"}} src={info.photo} alt=""/>
                    <span style={{paddingLeft:"15px"}}>
                          <h1>{dish}</h1>
                    <h3>cost: {info.cost} KZ</h3>
                    </span>
                    <button className="ButtonAdd" onClick={() =>this.props.productAdd( dish,info.cost)}>Add to cart</button>
                </div>
            })
        }
        return (
            <div style={{width: "500px", height:"500px", border:" 2px solid"}}>
                { this.props.loading?<div className="allSpin"><Spinner/></div>:null}
                <div>
                    {
                        dish
                    }
                </div>

            </div>

        )
    }
}
const mapStateToProps = state =>{
    return{
        dishes:state.dishes.dishes,
        loading: state.dishes.loading,
    }
};
const mapDispatchToProps = dispatch =>{

    return{
        takeDishes: () => dispatch(takeDishes()),
        productAdd: (info, dish) => dispatch(addProduct(info, dish))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Dishes);
