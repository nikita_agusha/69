import React from 'react';
import './App.css';
import Dishes from "./Dishes/Dishes";
import Cart from "./Cart/Cart";

function App() {
  return (
    <div className="App" >
     <Dishes/>
     <Cart/>
    </div>
  );
}

export default App;
