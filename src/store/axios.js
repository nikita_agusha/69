import axios from "axios";

const  instance = axios.create({
    baseURL:"https://classwork63-33496.firebaseio.com"
});
export  default  instance;