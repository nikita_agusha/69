import {
    CLOSE_MODAL,
    CREATE_ORDER, CREATE_ORDER_ERROR,
    CREATE_ORDER_REQUEST, CREATE_ORDER_SUCCESS,
    OPEN_MODAL,
    PRODUCT_ADD,
    PRODUCT_DELETE,
    TAKE_INFO_USER
} from "../cartAction";

const initialState ={
    cart:{},
    totalPrice:100,
    purchasing:false,
    loading:false,
    infoUser:{
        name:"",
        tel:"",
        address:""
    },
    error:null
};

const reducer = (state=initialState, action) =>{
    switch (action.type) {
        case PRODUCT_ADD:
            const copyState = {...state};
            const newPrice=copyState.totalPrice + action.cost;
            let cart = {cost: action.cost, cal: 1};
            if (action.name in copyState.cart) {
                state.cart[action.name].cal++

            } else {
                copyState.cart[action.name] = cart
            }
            return {...state, cart: copyState.cart, delivery: copyState.delivery, totalPrice: newPrice  };

        case PRODUCT_DELETE:
            const copy = {...state};
            const delNewPrice =copy.totalPrice - action.info.cost;
            if ( action.info.cal > 1 ) {
                state.cart[action.name].cal--

            } else if(action.info.cal === 1) {
               delete copy.cart[action.name]
            }
            return {...state, cart: copy.cart, delivery: copy.delivery, totalPrice: delNewPrice  };
        case OPEN_MODAL:
            return {...state, purchasing: true};
        case CLOSE_MODAL:
            return {...state, purchasing: false, infoUser:{name:"", tel:"", address:""}};
        case CREATE_ORDER:
            return  state;
        case TAKE_INFO_USER:
            let stateCopy ={...state};
            stateCopy.infoUser={...state.infoUser,[action.name]:action.value};
            return {...state, infoUser: stateCopy.infoUser};
        case CREATE_ORDER_REQUEST:
            return {...state, loading: true};
        case CREATE_ORDER_SUCCESS:
            return {...state, infoUser:{name:"", tel:"", address:""} , purchasing:false, loading: false};
        case CREATE_ORDER_ERROR:
            return {...state, error: action.error, loading: false};
        default:
            return state
    }
};
export default reducer;