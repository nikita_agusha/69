import {TAKE_DISHES_ERROR, TAKE_DISHES_REQUEST, TAKE_DISHES_SUCCESS} from "../dishesAction";

const initialState ={
    dishes:{},
    loading:false,
    error:null,
};

const reducer = (state=initialState, action) =>{
    switch (action.type) {

        case TAKE_DISHES_REQUEST:
            return {...state, loading: true};
        case TAKE_DISHES_SUCCESS:
            return {...state, dishes: action.dishes, loading: false};
        case TAKE_DISHES_ERROR:
            return {...state,loading: false};
        default:
            return state
    }
};
export default reducer;