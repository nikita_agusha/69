import axios from "./axios";
import {TAKE_DISHES_ERROR, TAKE_DISHES_REQUEST, TAKE_DISHES_SUCCESS} from "./dishesAction";
import {
    CLOSE_MODAL,
    CREATE_ORDER_ERROR, CREATE_ORDER_REQUEST, CREATE_ORDER_SUCCESS,
    OPEN_MODAL,
    PRODUCT_ADD,
    PRODUCT_DELETE,
    TAKE_INFO_USER
} from "./cartAction";



const  fetchDishesRequest = () =>{
    return {type: TAKE_DISHES_REQUEST};
};

const fetchDishesSuccess = (dishes) =>{
    return {type: TAKE_DISHES_SUCCESS, dishes};
};


const fetchDishesError =(error) =>{
    return {type: TAKE_DISHES_ERROR, error}
};

export const addProduct =(name, cost) =>{
    return {type: PRODUCT_ADD, name, cost}
};

export const deleteProduct =(name,info) =>{
    return {type: PRODUCT_DELETE, name,info}
};
export const closeModal = () =>{
    return{type:CLOSE_MODAL}
};
export const openModal = () =>{
    return{type:OPEN_MODAL}
};
const  createOrderRequest = () =>{
    return {type: CREATE_ORDER_REQUEST};
};

const createOrderSuccess = (dishes) =>{
    return {type: CREATE_ORDER_SUCCESS, dishes};
};


const createOrderError =(error) =>{
    return {type: CREATE_ORDER_ERROR, error}
};
export const createOrder = ( order,infoUser) =>{
    let calProduct=" ";
    let carts = Object.keys(order).map(cart => {
        const inf = order[cart];
        calProduct=inf.cal;
        return{name:cart, cal:inf.cal}
    });
        return dispatch =>{
            dispatch(createOrderRequest());
            axios.post ("/dishOrder.json",{
                userInfo:infoUser,
                order:carts
            }).then(res =>{
                dispatch(createOrderSuccess(res.data));
            }).catch(error =>{
                dispatch(createOrderError(error))
            })
    }

};

export const takeUserInfo = (name, value) =>{
    return{type:TAKE_INFO_USER , name,value}
};
export const takeDishes = () =>{
    return dispatch =>{
        dispatch(fetchDishesRequest());
        axios.get ("/dishes.json").then(res =>{
            dispatch(fetchDishesSuccess(res.data));
        }).catch(error =>{
            dispatch(fetchDishesError(error))
        })
    }
};

