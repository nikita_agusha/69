import React from "react";
import "./Modal.css";
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
  return (
    <>
      <Backdrop
        show={props.show}
        clicked={props.closed}
      />
      <div
        style={{
          transform: props.show ? "translateY(0px)" : "translateY(-100vh)",
          opacity: props.show ? 1 : 0
        }}
        className="Modal"
      >
          <input
              onChange={(e)=>props.takeInfo(e.target.name,e.target.value )}
              required value={props.infoUser.name}
              style={{marginBottom:"10px"}}
              name="name"
              type="text"
              placeholder="Name"/>
          <input
              onChange={(e)=>props.takeInfo(e.target.name, e.target.value)}
              required value={props.infoUser.tel}
              style={{marginBottom:"10px"}}
              name="tel"
              type="text"
              placeholder="tel"/>
          <input
              onChange={(e)=>props.takeInfo(e.target.name, e.target.value)}
              required value={props.infoUser.address}
              style={{marginBottom:"10px"}}
              name="address"
              type="text"
              placeholder="address"/>
          <button
              onClick={() => props.createOrder(props.cart, props.infoUser)}
                style={{height:"35px"}}
          >create Order</button>
      </div>
    </>
  );
};

export default Modal;
